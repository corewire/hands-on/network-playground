# Corewire - Training and Consulting - Demo Application

This is a simple alpine image with some additional tools to enable some
network debugging. It is part of a hands-on in the [Docker training](https://labs.corewire.de/de/docker/) by [corewire](https://corewire.de/).
