# This is a simple alpine image with some additional tools to enable some
# network debugging. It is used in hands-on parts of the corewire Docker
# training.
FROM alpine:latest

RUN apk --update --no-cache add \
    bash \
    tcpdump